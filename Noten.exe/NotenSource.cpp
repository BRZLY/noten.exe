  //-------------------------------//
 //  Programm f�r Noten�bersicht  //
//-------------------------------//

#include <iostream>
#include<csignal>
#include <stdlib.h>
using namespace std;

//Prototypen
int Menue(void);
void NotenAusgeben(int noten[]);  //Men�point1
int NotenEinlesen(int noten[]);  //Men�point2
void NotenLoeschen(int noten[]);//Men�point3

//Globale Definiton
int aiNoten[6] = { 0 };
int NotenEingabe = 0;
bool wiederholung = 1;
char YN;


//Hauptprogramm
int main()
{
	
	

	while ( wiederholung = true)
	{
		Menue();
		cout << "Welche Funktion wollen sie verwenden?" << endl;
		cin >> NotenEingabe;
		cout << "\n";

		if (NotenEingabe == 0)
		{
			system("exit");
		}
		
		if (NotenEingabe == 1)
		{
			NotenAusgeben(aiNoten);
		}

		if (NotenEingabe == 2)
		{
			NotenEinlesen(aiNoten);
		}

		if (NotenEingabe == 3)
		{
			NotenLoeschen(aiNoten);
		}

		cout << "Wollen sie von vorne beginnen???\n" << endl;
		cout << "JA = Y" << endl << "Nein = N\n" << endl;
		cin >> YN;
		if (YN == 'N')
		{
			wiederholung = false;
		}


		system("CLS");

	}
	getchar();
	return 0;

}

//Funktionen
int Menue(void)
{
	cout << "---------------------------Noten.exe---------------------------"<< endl;
	cout << "|                                                             |"<< endl;
	cout << "|                    0 = Programm schliesen                   |"<< endl;
	cout << "|                    1 = Noten ausgeben                       |"<< endl;
	cout << "|                    2 = Noten einlesen                       |"<< endl;
	cout << "|                    3 = Noten loeschen                       |"<< endl;
	cout << "|                                                             |"<< endl;
	cout << "---------------------------------------------------------------\n"<< endl;

	return 0;
}

void NotenAusgeben(int noten[])
{
	cout << "Noten Ausgeben funzt \n" << endl;
}

int NotenEinlesen(int noten[])
{
	cout << "Noten Einlesen funzt \n" << endl;
	return 0;
}

void NotenLoeschen(int noten[])
{
	cout << "Noten Loeschen funzt \n" << endl;
	
}
